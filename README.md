技术选型：
spring 4.1
springMVC 4.1
netty 4
java8
jackson 2.3
logback
mybatis
ssdb
druid数据库连接池
test
test

缩写
social-touch  st
haier interactive treasure HIT

说明：
自建连接本地数据库 表结构：
user_id ,user_name,real_name,phone,title

ssdb依赖 在本地私服 可以手动放入 请查看／test/resources/ssdbj-1.0.2.jar pom
api使用说明
http://git.oschina.net/jbakwd/ssdbj#git-readme

测试地址：
http://localhost:8070/hello/test/67?body=XXX

运行：
直接运行main函数
或者使用maven打包后 java －jar HITApi.jar [port env 端口号和spring环境 不指定默认使用配置文件中配置]

错误代码说明：
请尽量统一采用读取配置文件的方式
除了自定义的错误代码，其他的错误代码，请参照http错误码

ssdb数据类型说明：
 * 从官方Java的API来看http://ssdb.io/docs/java/index.html
 * 支持KV,hashmap,sortSet类型的数据,经过测试也支持list类型的数据 命令参考http://ssdb.io/docs/zh_cn/commands/index.html
 * multi操作 只支持kv类型 经过试验 可以支持map set 的multi_set操作，但是不支持multi_get操作 可以用scan命令代替
 * sortSet类型只能存放double类型的score
 
 2015-9-9  项目结构更改
 取消了dao层  加入mapper层  pojo  拆分成bo,po，自动扫描po下的实体作mybatis 别名 取消数据层操作的泛型限制
 
 post请求方式扩展：（举例）
 http://localhost:8070/hello/testPage/10/1?x1=1&x=2&x3=3
 request body填写：
{"pageSize":10,"pageNum":2}

关于controller中@RequestMapping标记的方法，对于运行时异常，如果不是要定义特别的错误代码
不需要使用try catch捕捉，已经在BaseController中统一处理

推荐@RestController注释
注解本身使用@Controller和@ResponseBody注解。使用了这个注解的类会被看作一个controller-使用@RequestMapping的方法有一个默认的@ResponseBody注解。

关于分页拦截器
指定mybatis的返回结果是hashmap时 过滤一遍去掉row_id 

