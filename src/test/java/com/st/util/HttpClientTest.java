package com.st.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.junit.Test;

import com.st.api.util.HttpClientUtils;

public class HttpClientTest {

	// @Test
	// public void testHttpClientNoTimeOut() {
	// long start = System.currentTimeMillis();
	// try {
	// PostMethod postMethod = new PostMethod("http://www.baidu.com");
	// HttpClientUtils.httpClientNoTimeOut.executeMethod(postMethod);
	// System.out.println(postMethod.getResponseBodyAsString());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// System.out.println(System.currentTimeMillis() - start);
	// }

//	@Test
//	public void testHttpClient() {
//		long start = System.currentTimeMillis();
//		try {
//			PostMethod postMethod = new PostMethod("http://127.0.0.1:8070/hello/testPage/10/1");
//			postMethod.setParameter("pageSize", "5");
//			postMethod.setParameter("currentPage", "1");
//			HttpClientUtils.httpClient.executeMethod(postMethod);
//			System.out.println(postMethod.getResponseBodyAsString());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		System.out.println(System.currentTimeMillis() - start);
//	}
	
	public static void main(String[] args) {
	    /*String getURL = "http://27.223.99.197:8070/login/authUser";
	    String params = "{\"userid\":\"00000007\",\"password\":\"Test,123\"}";
	    String s = urlPostMethod(getURL, params);*/
		String getUrl = "http://27.223.99.197:8070/system/getEcList?token=b26be0aa80d69d148b42a49de8506786&uid=00000007";
		String s = urlGetMethod(getUrl);
		
	    System.out.println(s);
	}

	
	/** * 获取post请求响应 * @param url * @param params * @return */
	public static String urlPostMethod(String url, String params) {
		HttpClient httpClient = new HttpClient();
		PostMethod method = new PostMethod(url);
		try {
			if (params != null && !params.trim().equals("")) {
				RequestEntity requestEntity = new StringRequestEntity(params, "application/json", "UTF-8");
				method.setRequestEntity(requestEntity);
			}
			method.releaseConnection();
			httpClient.executeMethod(method);
			String responses = method.getResponseBodyAsString();
			return responses;
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** * 获取post请求响应 * @param url * @param params * @return */
	public static String urlGetMethod(String url) {
		HttpClient httpClient = new HttpClient();
		GetMethod method = new GetMethod(url);
		try {
			method.getParams().setContentCharset("UTF-8");
			method.releaseConnection();
			httpClient.executeMethod(method);
			InputStream inputStream = method.getResponseBodyAsStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);
			String str = null;
			StringBuffer responses = new StringBuffer();
			while ((str = bufferedReader.readLine()) != null) {
				responses.append(str);
			}
			return responses.toString();
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

}
