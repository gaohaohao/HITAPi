package java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class LambdaUseTest {

	public static void main(String[] args) {

		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
		
		numbers.forEach((Integer value) -> System.out.println(value));
		numbers.forEach(bean -> {System.out.println(bean);});

		sumAll(numbers, n -> true);
		sumAll(numbers, n -> n % 2 == 0);
		sumAll(numbers, n -> n > 3);

		Resource.withResource(resource -> resource.operate());
	    
	}
	
	public static int sumAll(List<Integer> numbers, Predicate<Integer> p) {
		int total = 0;
		for (int number : numbers) {
			if (p.test(number)) {
				total += number;
			}
		}
		return total;
	}
}
