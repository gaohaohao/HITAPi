package java8;

public class Person {
	
	public static Integer MALE = 0;
	public static Integer FEMALE = 2;
	
	private Integer sex;
	private Integer age;
	

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
