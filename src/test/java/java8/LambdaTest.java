package java8;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.apache.commons.math3.primes.Primes;


/**
 * 表达式有三部分组成：参数列表，箭头（->），以及一个表达式或语句块。 本质上是一个匿名方法 目标类型是函数接口
 * 一个λ表达式其实就是定义了一个匿名方法，只不过这个方法必须符合至少一个函数接口
 * 
 * @author wuxianwei
 *
 */
public class LambdaTest {

	public static void main(String[] args) throws Exception {

		Runnable r1 = () -> {
			System.out.println("Hello Lambda!");
		};
		r1.run();
		Object obj = r1;

		// Object obj = () -> {System.out.println("Hello Lambda!");}; // ERROR!
		// Object is not a functional interface!

		Object o = (Runnable) () -> {
			System.out.println("hi");
		}; // correct

		// System.out.println( () -> {} ); //错误! 目标类型不明

		System.out.println((Runnable) () -> {
		}); // 正确

		Thread oldSchool = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("This is from an anonymous class.");
			}
		});
		oldSchool.start();

		Thread gaoDuanDaQiShangDangCi = new Thread(() -> {
			System.out.println("This is from an anonymous method (lambda exp).");
		});
		gaoDuanDaQiShangDangCi.start();

		// 配合“集合类批处理操作”的内部迭代和并行处理
		List<String> stringList = Arrays.asList("a", "ba", "ca", "d", "e", "f");
		stringList.stream().filter(str -> {
			return str.contains("a");
		}).forEach(str -> {
			String str1 = str.replace("a", "x");
			System.out.println(str1);
		});
		//指明要并行处理，以期充分利用现代CPU的多核特性
		//shapes.parallelStream();或shapes.stream().parallel()
		
		distinctPrimary("1","2","3","4","5","6");
		
		 // 嵌套的λ表达式
	    Callable<Runnable> c1 = () -> () -> { System.out.println("Nested lambda"); };
	    c1.call().run();

	    // 用在条件表达式中
	    Callable<Integer> c2 = true ? (() -> 42) : (() -> 24);
	    System.out.println(c2.call());


	    System.out.println(factorial.apply(3));
	}
	// 定义一个递归函数，注意须用this限定  这个有点晕TODO
	protected static UnaryOperator<Integer> factorial = i -> i == 0 ? 1 : i * LambdaTest.factorial.apply( i - 1 );
	
	 public static void distinctPrimary(String... numbers) {
		 	System.out.println("Start distinct -----------------------");
		 
	        List<String> l = Arrays.asList(numbers);
	        
	        Map<String, Integer> map = l.stream().map(e -> {return Integer.parseInt(e);}).
	        filter( i -> i > 2).distinct().
	        collect(Collectors.toMap(i -> i+"x", i -> i+1));
	        Set<String> setKey = map.keySet();
	        setKey.stream().forEach(key -> {
	        	System.out.println(key+":"+map.get(key));
	        });
	        
	        List<Integer> r = l.stream()
	                .map(e -> new Integer(e))
	                .filter(e -> e>1)
	                .distinct()
	                .collect(Collectors.toList());
	        System.out.println("distinctPrimary result is: " + r);
	    }
	 
	//给出一个String类型的数组，找出其中各个素数，并统计其出现次数
	    public void primaryOccurrence(String... numbers) {
	        List<String> l = Arrays.asList(numbers);
	        Map<Integer, Integer> r = l.stream()
	            .map(e -> new Integer(e))
	            .filter(e -> Primes.isPrime(e))
	            .collect( Collectors.groupingBy(p->p, Collectors.summingInt(p->1)) );
	        //把结果收集到一个Map中，用统计到的各个素数自身作为键，其出现次数作为值。
	        System.out.println("primaryOccurrence result is: " + r);
	    }
	    
		//方法引用
		public void distinctPrimarySumBY(String... numbers) {
	        List<String> l = Arrays.asList(numbers);
	        int sum = l.stream().map(Integer::new).filter(Primes::isPrime).distinct().reduce(0, (x,y) -> x+y);
	        System.out.println("distinctPrimarySum result is: " + sum);
	    }

	    //给出一个String类型的数组，求其中所有不重复素数的和
	    public void distinctPrimarySum(String... numbers) {
	        List<String> l = Arrays.asList(numbers);
	        int sum = l.stream()
	            .map(e -> new Integer(e))
	            .filter(e -> Primes.isPrime(e))
	            .distinct()
	            .reduce(0, (x,y) -> x+y); // equivalent to .sum()
	        //reduce方法用来产生单一的一个最终结果。
	        //流有很多预定义的reduce操作，如sum()，max()，min()等。
	        System.out.println("distinctPrimarySum result is: " + sum);
	    }
	    
	    // 统计年龄在25-35岁的男女人数、比例
	    public void boysAndGirls(List<Person> persons) {
	        Map<Integer, Integer> result = persons.parallelStream().filter(p -> p.getAge()>=25 && p.getAge()<=35).
	            collect(
	                Collectors.groupingBy(p->p.getSex(), Collectors.summingInt(p->1))
	        );
	        System.out.print("boysAndGirls result is " + result);
	        System.out.println(", ratio (male : female) is " + (float)result.get(Person.MALE)/result.get(Person.FEMALE));
	    }
}
