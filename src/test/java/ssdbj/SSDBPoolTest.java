package ssdbj;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;

import com.lovver.ssdbj.core.BaseResultSet;
import com.lovver.ssdbj.core.impl.SSDBConnection;
import com.lovver.ssdbj.pool.SSDBDataSource;
import com.lovver.ssdbj.pool.SSDBPoolConnection;

/**
 * 从官方Java的API来看http://ssdb.io/docs/java/index.html
 * 支持KV,hashmap,sortSet类型的数据,经过测试也支持list类型的数据 命令参考http://ssdb.io/docs/zh_cn/commands/index.html
 * multi操作 只支持kv类型 经过试验 可以支持map set 的multi_set操作，但是不支持multi_get操作 可以用scan命令代替
 * sortSet类型只能存放double类型的score
 * @author wuxianwei
 *
 */
public class SSDBPoolTest {
	static SSDBDataSource ds = null;

	static {
		Properties info = new Properties();
		info.setProperty("password", "ddd");
		info.setProperty("loginTimeout", "300");
		info.setProperty("tcpKeepAlive", "true");
		info.setProperty("protocolName", "ssdb");
		info.setProperty("protocolVersion", "1.0");
		ds = new SSDBDataSource("127.0.0.1", 8888, null, info);
	}
	@Test
	public void testprintln(){
		System.out.println("joliny".getBytes());
	}

	/**
	 * @param args
	 * @throws Exception
	 */
//	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws Exception {
		SSDBPoolConnection conn = null;
		conn = ds.getConnection();
		
		ArrayList<byte[]> setparamsMulti = new ArrayList<byte[]>() {
			{
				add("hname".getBytes());
				add("k1".getBytes());
				add("中文".getBytes());
				add("k2".getBytes());
				add("English".getBytes());
				add("k3".getBytes());
				add("123".getBytes());
			}
		};
		BaseResultSet<byte[]> rsMulti = conn.execute("multi_hset", setparamsMulti);
		//注意 执行set  不返回结果 不报错就代表成功
		System.out.println(rsMulti);
		//multi_hget 命令不可用
		
		ArrayList<byte[]> hscan_params = new ArrayList();
		hscan_params.add("hname".getBytes());
		hscan_params.add("".getBytes());
		hscan_params.add("".getBytes());
		hscan_params.add("10".getBytes());
		BaseResultSet<Map<byte[], byte[]>> Hscan_rs = conn.execute("hscan", hscan_params);
		
		ArrayList<byte[]> zsetparamsMulti = new ArrayList<byte[]>() {
			{
				add("zname".getBytes());
				add("k1".getBytes());
				add("4".getBytes());
				add("k2".getBytes());
				add("1".getBytes());
				add("k3".getBytes());
				add("123".getBytes());
			}
		};
		BaseResultSet<byte[]> zrsMulti = conn.execute("multi_zset", zsetparamsMulti);
		
		

		ArrayList<byte[]> qsetparamsMulti = new ArrayList<byte[]>() {
			{
				add("qname".getBytes());
				add("k1".getBytes());
				add("4".getBytes());
				add("k2".getBytes());
				add("1".getBytes());
				add("k3".getBytes());
				add("123".getBytes());
			}
		};
		BaseResultSet<byte[]> qrsMulti = conn.execute("qpush_back", qsetparamsMulti);
		
		for (int i = 0; i < 10; i++) {
			try {
				conn = ds.getConnection();

				ArrayList<byte[]> setparams = new ArrayList<byte[]>() {
					{
						add("joliny".getBytes());
						add("kkk".getBytes());
						add("是的发生地发生1231sdfsfg23".getBytes());
					}
				};

				BaseResultSet<byte[]> rs0 = conn.execute("hset", setparams);
//				System.out.println(rs0.getResult());

				ArrayList params = new ArrayList();
				params.add("joliny".getBytes());
				params.add("kkk".getBytes());
				BaseResultSet<byte[]> rs = conn.execute("hget", params);
				System.out.println(new String(rs.getResult()));

				ArrayList mset_params = new ArrayList();
				mset_params.add("a".getBytes());
				mset_params.add("aaaaa1".getBytes());
				mset_params.add("b".getBytes());
				mset_params.add("bbbbbb2".getBytes());
				conn.executeUpdate("multi_set", mset_params);

				ArrayList mget_params = new ArrayList();
				mget_params.add("a".getBytes());
				mget_params.add("b".getBytes());

				BaseResultSet<Map<byte[], byte[]>> m_rs = conn.execute("multi_get", mget_params);
				Map<byte[], byte[]> items = m_rs.getResult();
				Iterator<byte[]> ite = items.keySet().iterator();
				while (ite.hasNext()) {
					byte[] key = ite.next();
					System.out.println(new String(key) + "=====" + new String(items.get(key)));
				}

				ArrayList<byte[]> scan_params = new ArrayList();
				scan_params.add("".getBytes());
				scan_params.add("".getBytes());
				scan_params.add("10".getBytes());
				BaseResultSet<Map<byte[], byte[]>> scan_rs = conn.execute("scan", scan_params);
				Map<byte[], byte[]> scan_items = scan_rs.getResult();
				Iterator<byte[]> scan_ite = scan_items.keySet().iterator();
				while (scan_ite.hasNext()) {
					byte[] key = scan_ite.next();
					System.out.println(new String(key) + "=====" + new String(scan_items.get(key)));
				}
			} finally {
				conn.close();
			}
		}
	}

}
