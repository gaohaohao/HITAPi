package intercept;

import javax.xml.bind.PropertyException;

public class MainTest {
	
	public static void main(String[] args) {
		System.out.println("--------------->>>"+1);
			try {
				System.out.println("--------------->>>"+2);
				throw new PropertyException("dialectName or dialect property is not found!");
			} catch (PropertyException e) {
				e.printStackTrace();
			}
			try {
				System.out.println("--------------->>>"+3);
				throw new PropertyException("pageSqlId property is not found!");
			} catch (PropertyException e) {
				e.printStackTrace();
			}
	}

}
