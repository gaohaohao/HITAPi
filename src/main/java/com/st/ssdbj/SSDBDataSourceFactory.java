package com.st.ssdbj;

import java.util.Properties;

import com.lovver.ssdbj.pool.SSDBDataSource;

public class SSDBDataSourceFactory {
	private SSDBDataSourceFactory() {
	}

	private static SSDBDataSource ssdbDataSource;

	//初始化 双重校验锁
	public static void init(String ssdbHost, int ssdbPort,String ssdbUser,Properties properties) {
		if (ssdbDataSource == null) {
			synchronized (SSDBDataSourceFactory.class) {
				if (ssdbDataSource == null) {
					ssdbDataSource = new SSDBDataSource(ssdbHost, ssdbPort, ssdbUser, properties);
				}
			}
		}
	}
	
	public static void destroy(){
		ssdbDataSource = null;
	}


	public static SSDBDataSource getSsdbDataSource() {
		return ssdbDataSource;
	}

}
