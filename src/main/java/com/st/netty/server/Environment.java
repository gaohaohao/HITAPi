package com.st.netty.server;

/**
 * 
 * @author wuxianwei
 *
 */
public enum Environment {
	/** 开发环境 **/
	development, /** 测试环境 **/
	test, /** 生产环境 **/
	production
}
