package com.st.api.bo;

public class PullUserModel implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1876055173877970729L;
	private String uid;
	private String sendTo;
	private String subject;
	private String content;
	private String conditon;
	private int countnum;
	
	public PullUserModel() {
		
	}

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getConditon() {
		return conditon;
	}

	public void setConditon(String conditon) {
		this.conditon = conditon;
	}

	public int getCountnum() {
		return countnum;
	}

	public void setCountnum(int countnum) {
		this.countnum = countnum;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	

}
