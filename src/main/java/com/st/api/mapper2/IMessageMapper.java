package com.st.api.mapper2;

import java.util.List;
import java.util.Map;

public interface IMessageMapper {

	/**
	 * 从序列 scrmappusr.seq_jhb_offer 中取得一个活动ID
	 * @return
	 */
	int getActId();
	/**
	 * 插入到jof表
	 * @param paramMap
	 */
	void insertJOF(Map paramMap);
	/**
	 * 插入jhb_offer
	 * @param userMapList
	 */
	void insertJCUST(List<Map> userMapList);
	/**
	 * 将测试手机号插入表中
	 * @param mobileList
	 */
	void insertTestMobile(List<Object> mobileList);

}
