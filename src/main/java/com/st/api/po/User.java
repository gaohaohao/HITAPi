package com.st.api.po;

import java.io.Serializable;
import java.util.Date;

public class User  implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -9022573204428880178L;
    
    private Integer userId;
    private String userName;
    private String userPassword;
    
    private Date createDate = new Date();
    
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
    
    
}
