package com.st.api.mapper;

import java.io.Serializable;
import java.util.List;

/**
 * 通用操作接口
 * 取消对操作po泛型的限制
 * CRDU operations
 * @author wuxianwei
 * @date 2015年7月11日 下午7:30:56
 */
public interface IBaseMapper{

    /**
     * @param t
     *            不允许为null
     */
    <T> int insert( T t) ;

    /**
     * @param statementId
     *            不允许为null
     * @param list
     *            不允许为null
     */
    <T extends Serializable> int insertBatch( List<T> list) ;

    /**
     * @param statementId
     *            不允许为null
     * @param parameters
     *            允许为null
     */
    <S> int delete( S parameters) ;

    /**
     * 根据参数list批量删除对象
     * @param statementId
     *            不允许为null
     * @param list
     *            不允许为null
     */
    <S> int deleteBatch( List<S> list) ;

    /**
     * 根据对象更新对象
     * @param t
     *            不允许为null
     */
    <T extends Serializable> int update( T t) ;

    /**
     * 根据参数更新对象
     * @param statementId
     *            不允许为null
     * @param parameters
     *            允许为null
     */
    <S> int update( S parameters) ;

    /**
     * @param statementId
     *            不允许为null
     * @param list
     *            不允许为null
     */
    <T extends Serializable> int updateBatch( List<T> list) ;

    /**
     * 根据参数获取对象
     * @param t
     *            不允许为null
     */
    <T extends Serializable> T query( T t) ;

    /**
     * 根据主键获取对象
     * @param id
     *            不允许为null
     */
    <T extends Serializable> T queryByPK( Integer parameters) ;

    /**
     * 无条件查询，查出所有
     * @author wuxianwei 
     * @param statementId
     * @return
     */
    <U> List<U> queryList(String statementId);

    /**
     * @param statementId
     *            不允许为null
     * @param parameters
     *            允许为null
     */
    <U, S> List<U> queryListWithParameter( S parameters) ;
    
}
