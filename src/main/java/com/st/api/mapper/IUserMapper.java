package com.st.api.mapper;

import java.util.List;
import java.util.Map;

import com.st.api.bo.Page;
import com.st.api.po.BasePo;

/**
 * mapper文件中的namespace写接口全名
 * sql的id和对应的方法名相同
 * @author wuxianwei
 *
 */

public interface IUserMapper extends IBaseMapper {
 
	/**
	 * 判断用户是否是第一次登录
	 * @param userid
	 * @return
	 */
	public int isFirstLogin(String userid);

	public List<Object> queryListForAll();

	public List<Object> queryUserPage(Page page);

	public List<Object> selectUserPage(Page page);

	

	
	/**
	 * 插入登录用户详细信息
	 * @param userid
	 * @param ipAddr
	 */
	public void insertSysLoginDetail(Map<String, String> insertParamMap);

	/**
	 * 是否W_SYS_USER_ROLE_REL中已存在用户的信息
	 * @param string
	 */
	public int hasInSertUserInfo(String userid);

	/**
	 * 向W_SYS_USER_ROLE_REL中插入用户信息
	 * @param userinfoMap
	 */
	public void insertUserInfo(Map<String, String> userinfoMap);

	public void updateUserInfoLastLoginTime(Map<String, String> userinfoMap);

	/**
	 * 获取app版本
	 * @return
	 */
	public Map<String, Object> getAppVersion();
}
