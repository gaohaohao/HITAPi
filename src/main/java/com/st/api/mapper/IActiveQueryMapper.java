package com.st.api.mapper;

import java.util.List;
import java.util.Map;

public interface IActiveQueryMapper {

	
	/**
	 * 根据省份查询城市列表
	 * @param prv_id
	 * @return
	 */
	public List<Object> getCityByProvince(Integer prvId);
	/**
	 * 根据城市等级查询城市列表
	 * @param levelId
	 * @return
	 */
	public List<Object> getCityByLevel(Integer levelId);

	/**
	 * 根据查询条件获取人员列表
	 * @param paramMap
	 * @return
	 */
	public List<Object> getUserList(Map paramMap);
	/**
	 * 获取社会化数据海尔
	 * @param paramMap
	 * @return
	 */
	public List<Object> getWIDHaier(Map paramMap);
	/**
	 * 获取社会化达人报表数据
	 * @return
	 */
	public List<Object> getDomainData();
	/**
	 * 获取社会化数据领域达人微博ID
	 * @return
	 */
	public List<Object> getDomainUserList(Map paramMap);
	
	/**
	 * 获取收入分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getIncomeDistribution(Map paramMap);
	/**
	 * 获取活跃度分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getActivityDistribution(Map<String, Object> paramMap);
	/**
	 * 获取年龄分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getAgeDistribution(Map<String, Object> paramMap);
	/**
	 * 性别分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getSexDistribution(Map<String, Object> paramMap);
}
