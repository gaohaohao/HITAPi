package com.st.api.mapper;

import java.util.List;
import java.util.Map;

public interface ISystemQueryMapper {

	/**
	 * 更新生态圈和产品大类
	 * @param uid
	 * @param ecid
	 * @param prodcat
	 * @return
	 */
	public void updateEcInfo(Map paramMap);

	/**
	 * 获取生态圈列表
	 * @return
	 */
	public List<Object> getEcList();

	/**
	 * 获取产品大类列表
	 * @param ecid
	 * @return
	 */
	public List<Object> getProdCatListByRole(Map paramMap);

	/**
	 * 获取收入等级列表
	 * @return
	 */
	public List<Object> getIncList();

	/**
	 * 获取生态圈列表（当前用户）
	 * @param uid
	 * @return
	 */
	public List<Object> getEcListByRole(String uid);

	/**
	 * 获取产品大类列表
	 * @return
	 */
	public List<Object> getProdCatList(Map paramMap);

	/**
	 * 更新职能
	 * @param rcMap
	 */
	public void updateRcInfo(Map<String, Object> rcMap);

	/**
	 * 通过uid查询出产品大类
	 * @param uid
	 * @return
	 */
	public List<Object> getProdCatByUserCode(String uid);

	/**
	 * 通过uid查处用户功能菜单
	 * @param uid
	 * @return
	 */
	public List<Object> getUserMenuByUserCode(String uid);

	
	/**
	 * 通过usercode获取用户信息
	 * @param uid
	 * @return
	 */
	public Map<String, Object> getUserInfoByUserCode(String uid);

	/**
	 * 获取产品系列
	 * @param prodcat
	 * @return
	 */
	public List<Object> getProdSeries(String prodcat);
	
	/**
	 * 查询省份列表
	 * @return
	 */
	public List<Object> getProvince();

	/**
	 * 获取用户职能列表
	 * @return
	 */
	public List<Object> getRoleList();

	/**
	 * 获取访问信息的map
	 * @return
	 */
	public Map<String, Object> getAccessInfoMap(String uid);

	/**
	 * 
	 * @param paramMap
	 * @return
	 */
	public List<Object> getProdCatListByRole2(Map<String, Object> paramMap);
	/**
	 * 更新生态圈和产品大类
	 * @param ecMap
	 * @return
	 */
	public void updateAllEcInfo(Map<String, Object> ecMap);

	/**
	 * 通过condition查询用户列表
	 * @param conditionMap
	 * @return
	 */
	public List<Map> getUserListByCondition(Map<String, Object> conditionMap);

	/**
	 * 根据uid更新acvite_user_f
	 * @param userMapList
	 */
	public void updateAUFByCId(List<Map> userMapList);

	/**
	 * 
	 * @return
	 */
	public List<Object> getSysUserList(Map<String, Object> paramMap);

	public List<Object> getSysUserRole(String usercode);

	/**
	 * 删除用户角色
	 * @param object
	 */
	public void delUserRole(Object object);

	/**
	 * 给用户添加角色
	 * @param paramMap
	 */

	public void updateUserRole(List<Map> roleMapList);

	/**
	 * 删除用户
	 * @param usercode
	 */
	public void delUserDataByUC(String usercode);

	/**
	 * 删除用户角色
	 * @param usercode
	 */
	public void delUserRoleRelByUC(String usercode);

	public void delUserInfoByUC(String usercode);

	/**
	 * 获取活跃用户数的前5个部门
	 * @param paramMap
	 * @return
	 */
	public List<Object> getDeptUseTop5(Map<String, Object> paramMap);

	/**
	 * 获取用户使用情况
	 * @param paramMap
	 * @return
	 */
	public List<Object> userUseInfo(Map<String, Object> paramMap);

	/**
	 * 新增用户top5
	 * @param paramMap
	 * @return
	 */
	public List<Object> getIncreaseUserTop5(Map<String, Object> paramMap);

	/**
	 * 新增用户数据
	 * @param paramMap
	 * @return
	 */
	public List<Object> increaseUserInfo(Map<String, Object> paramMap);

	public List<Object> userRoleUseInfo(Map<String, Object> paramMap);

	public List<Object> increaseUserRoleInfo(Map<String, Object> paramMap);

	public List<Map> getUserListByConditionRandom(Map<String, Object> conditionMap);

	/**
	 * 删除其他人审核权限
	 */
	public void delSHMPower();

}
