package com.st.api.mapper;

import java.util.List;
import java.util.Map;

public interface ILifeMapper {

	/**
	 * 4.5.1	获取生活圈Tag列表
	 * @return
	 */
	List<Object> getLifeTag();

	
	/**
	 * 4.5.2	根据标签、性别查询其兴趣标签的大类
	 * @param tag
	 * @param sex
	 * @return
	 */
	List<Object> lifeInterestAnalysis(Map<String, Object> paramMap);

	/**
	 * 4.5.3	选择生活圈兴趣大类下钻到二级分类
	 * @param paramMap
	 * @return
	 */

	List<Object> lifeInterestItemCycle(Map<String, Object> paramMap);


	/**
	 * 4.5.4	选择生活圈兴趣分析钻探细分报表数据
	 * @param paramMap
	 * @return
	 */
	List<Object> lifeInterestItem(Map<String, Object> paramMap);

	/**
	 * 4.5.4查询生活圈关注媒体报表数据
	 * @param paramMap
	 * @return
	 */

	List<Object> lifeMedia(Map paramMap);


	/**
	 * 生活圈兴趣总数量
	 * @return
	 */
	int lifeInterestCount(Map paramMap);


	int getLifeMediaRecordCount(Map paramMap);



	/**
	 * /**
	 * 获取用户性别占比
	 */
	
	List<Object> getSexDistribution(Map<String, Object> paramMap);


	/**
	 * 获取用户地区占比
	 * @param paramMap
	 * @return
	 */
	List<Object> getAreaDistribution(Map paramMap);


	/**
	 * 媒体大V偏好分析
	 * @param page
	 * @param paramMap
	 * @return
	 */
	List<Object> lifeCommonPage(Map paramMap);


	/*List<Object> getCity();

	*//**
	 * 更新城市
	 * @param paramMap
	 *//*
	void testCity(Map paramMap);*/

	
}
