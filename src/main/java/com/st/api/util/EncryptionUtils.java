package com.st.api.util;

import java.security.MessageDigest;

import org.apache.commons.lang.StringUtils;

/**
 * 加密工具类 支持md5 和 sha 返回64位的十六进制的数字字母的混合的字符串
 * 
 * @author wuxianwei
 *
 */
public class EncryptionUtils {
	private EncryptionUtils() {

	}

	private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
			"e", "f" };

	/**
	 * 转换字节数组成16进制字串
	 * 
	 * @param b
	 *            字节数组
	 * @return 16进制字串
	 */
	private static String byteArrayToString(byte[] b) {
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			resultSb.append(byteToHexString(b[i]));//若使用本函数转换则可得到加密结果的16进制表示，即数字字母混合的形式
			// resultSb.append(byteToNumString(b[i]));//使用本函数则返回加密结果的10进制数字字串，即全数字形式
		}
		return resultSb.toString();
	}

	// private static String byteToNumString(byte b) {
	// int _b = b;
	// if (_b < 0) {
	// _b = 256 + _b;
	// }
	// return String.valueOf(_b);
	// }

	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0) {
			n = 256 + n;
		}
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}

	/**
	 * sha1 摘要转16进制
	 * 
	 * @param digest
	 * @return
	 */
	private static String toHex(byte[] digest) {
		StringBuilder sb = new StringBuilder();
		int len = digest.length;
		String out = null;
		for (int i = 0; i < len; i++) {
			out = Integer.toHexString(0xFF & digest[i]);// 原始方法
			if (out.length() == 1) {
				sb.append("0");// 如果为1位 前面补个0
			}
			sb.append(out);
		}
		return sb.toString();
	}

	public static String MD5Encode(String origin) {
		String resultString = null;

		try {
			resultString = new String(origin);
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byteArrayToString(md.digest(resultString.getBytes()));
			resultString = resultString.toUpperCase();
		} catch (Exception ex) {

		}
		return resultString;
	}

	public static String SHAEncode(String str) throws Exception {
		MessageDigest sha1;
		sha1 = MessageDigest.getInstance("SHA-256");
		sha1.update(str.getBytes()); // 先更新摘要
		byte[] digest = sha1.digest(); // 再通过执行诸如填充之类的最终操作完成哈希计算。在调用此方法之后，摘要被重置。
		String hex = toHex(digest);
		return StringUtils.reverse(hex);// 返回倒置
	}
	
	public static void main(String[] args) throws Exception {
		String userid = "haozi";
		String shaStr = SHAEncode(userid);
		System.out.println("shaStr::"+shaStr);
		
		System.out.println("-----------分割线----------------");
		
		String md5Str = MD5Encode(userid);
		
		System.out.println("md5Str::"+md5Str);
		
		System.out.println("------------分割线---------------");
		
		
	}

}
