package com.st.api.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.st.api.controller.ActiveQueryController;

public class StringUtils {

	
	private static Logger logger = LoggerFactory.getLogger(StringUtils.class);
	/**
	 * 判断字符串是否为空 如空返回True,否则返回false
	 * @param str
	 * @return
	 */
	public static boolean isNull(String str)
	{
		
		if(null==str||"".equals(str)||"null".equals(str)||"undefined".equals(str))
			return true;
		else
		return false;
	}
	
	/**
	 * 封装返回map
	 * @param status
	 * @param message
	 * @return
	 */
	public static Map<String,Object> returnMap(int status,String message){
		Map<String,Object> returnMap = new HashMap<String,Object>();
		returnMap.put("status", status);
		returnMap.put("errmsg", message);
		return returnMap;
	}
	
	/**
	 * json字符串转换为map
	 * @param jsonStr
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public static Map<String,String> jsonStrToMap(String jsonStr) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> paramMap = null;
		try {
			paramMap = mapper.readValue(jsonStr,Map.class);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return paramMap;
	}
	
	
	public static Map<String,Object> jsonStrToObjMap(String jsonStr) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> paramMap = null;
		try {
			paramMap = mapper.readValue(jsonStr,Map.class);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return paramMap;
	}
	
	/**
	 * map对象转换为json字符串
	 * @param strMap
	 * @return
	 */
	public static String mapToJsonStr(Map strMap){
		//map to json
		ObjectMapper mapper = new ObjectMapper(); 
		String jsonMap=null;
		try {
			jsonMap = mapper.writeValueAsString(strMap);
		} catch (JsonProcessingException e) {
			
			e.printStackTrace();
		}
		
		return jsonMap;
	}
	
	
	
	/**
	 * 从request中获得参数Map，并返回可读的Map
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map getParameterMap(HttpServletRequest request) {
	    // 参数Map
	    Map properties = request.getParameterMap();
	    // 返回值Map
	    Map returnMap = new HashMap();
	    Iterator entries = properties.entrySet().iterator();
	    Map.Entry entry;
	    String name = "";
	    String value = "";
	    while (entries.hasNext()) {
	        entry = (Map.Entry) entries.next();
	        name = (String) entry.getKey();
	        Object valueObj = entry.getValue();
	        if(null == valueObj){
	            value = "";
	        }else if(valueObj instanceof String[]){
	            String[] values = (String[])valueObj;
	            for(int i=0;i<values.length;i++){
	                value = values[i] + ",";
	            }
	            value = value.substring(0, value.length()-1);
	        }else{
	            value = valueObj.toString();
	        }
	        logger.info("request URI::{}",request.getRequestURI());
	        logger.info("request parameter:::::::::::::name:{},value:{}",name,value);
	        returnMap.put(name, value);
	    }
	    return returnMap;
	}
	
	/**
	 * 字符串转为int数组
	 * @param str
	 * @return
	 */
	public static int[] strToIntArray(String str,String regex){
		String[] strArray = str.split(regex);
		int[] intArray = new int[strArray.length];
		for(int i=0;i<intArray.length;i++){
			intArray[i] = Integer.valueOf(strArray[i]);
		}
		return intArray;
	}
	/**
	 * 字符串转Str数组
	 * @param str
	 * @param regex
	 * @return
	 */
	public static String[] strToStrArray(String str,String regex){
		
		
		return str.split(regex);
	}
	/**
	 * 字符串转化为日期
	 * @param dateStr
	 * @param datePattern
	 * @return
	 */
	public static Date strToDate(String dateStr,String datePattern){
		SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	/**
	* JAVA判断字符串数组中是否包含某字符串元素
	*
	* @param substring 某字符串
	* @param source 源字符串数组
	* @return 包含则返回true，否则返回false
	*/
	public static boolean isInArr(String substring, String[] source) {
		if (source == null || source.length == 0) {
			return false;
		}
		for (int i = 0; i < source.length; i++) {
			String aSource = source[i];
			if (aSource.equals(substring)) {
				return true;
			}
		}
		return false;
	}
	
}
