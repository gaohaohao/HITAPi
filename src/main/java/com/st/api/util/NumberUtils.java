package com.st.api.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class NumberUtils {

	
	
	public static String numberFormat2decimal(Double d){
		
		return new DecimalFormat("#.00").format(d);
	}
	public static double Double2point(double num){
		return new BigDecimal(num).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
		
	}
}
