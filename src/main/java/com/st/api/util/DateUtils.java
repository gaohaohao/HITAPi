package com.st.api.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * jsr310
 * 
 * @author wuxianwei
 *
 */
public class DateUtils {

	private static SimpleDateFormat dateStrSdf = new SimpleDateFormat("yyyy-MM-dd");

	private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	/**
	 * 返回标准日期格式
	 * 
	 * @param date
	 * @return
	 */
	public static String dateTimeToStr(Date date) {
		return dateStrSdf.format(date);
	}

	/**
	 * jsr返回格式化的日期
	 * 
	 * @param dateTime
	 * @return
	 */
	public static String dateTimeToStr(LocalDateTime dateTime) {
		return dateTimeFormatter.format(dateTime);
	}

	/**
	 * jsr 获取本地当前日期
	 * 
	 * @return
	 */
	public static String getDateStr() {
		return LocalDate.now().toString();
	}
}
