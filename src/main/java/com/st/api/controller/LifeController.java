package com.st.api.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.st.api.bo.FacadeResult;
import com.st.api.bo.Page;
import com.st.api.constants.ReturnServiceCode;
import com.st.api.service.ILifeService;
import com.st.api.util.CityUtil;
import com.st.api.util.StringUtils;

@RestController
@RequestMapping(value="/life",produces = "application/json")
@Scope("prototype")
public class LifeController extends BaseController{

	@Autowired
	private ILifeService lifeService;
	
	@RequestMapping("/getLifeTag")
	public @ResponseBody FacadeResult getLifeTag(HttpServletRequest request,HttpServletResponse response){
		
		List<Object> ret = lifeService.getLifeTag();
		
		return generateData(ret);
	}
	
	@RequestMapping("/lifeInterestAnalysis")
	public @ResponseBody FacadeResult lifeInterestAnalysis(HttpServletRequest request,HttpServletResponse response){
		
		if(StringUtils.isNull(request.getParameter("tag"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		
		if(!StringUtils.isNull(request.getParameter("sex"))){
			paramMap.put("sex", request.getParameter("sex"));
		}
		//cal life interest total count
		
		int totalCount = lifeService.lifeInterestCount(paramMap);
		
		paramMap.put("totalCount", totalCount);
		
		List<Object> ret = lifeService.lifeInterestAnalysis(paramMap);
		
		return generateData(ret,ret.size());
	}
	
	@RequestMapping("/lifeInterestItemCycle")
	public @ResponseBody FacadeResult lifeInterestItemCycle(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
		
		if(StringUtils.isNull(request.getParameter("tag"))||
					StringUtils.isNull(request.getParameter("typename"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
	
		paramMap.put("tag", Integer.valueOf(request.getParameter("tag")));
		
		if(!StringUtils.isNull(request.getParameter("sex"))){
			paramMap.put("sex", request.getParameter("sex"));
		}
		
		paramMap.put("typename", URLDecoder.decode(request.getParameter("typename"), "UTF-8"));
		
		int totalCount = lifeService.lifeInterestCount(paramMap);
		
		paramMap.put("totalCount", totalCount);
		
		List<Object> ret = lifeService.lifeInterestItemCycle(paramMap);
		
		return generateData(ret,ret.size());
	}
	
	@RequestMapping("/lifeInterestItem")
	public @ResponseBody FacadeResult lifeInterestItem(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
		
		if(StringUtils.isNull(request.getParameter("tag"))||
					StringUtils.isNull(request.getParameter("typename"))||
						StringUtils.isNull(request.getParameter("midtypename"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
	
		paramMap.put("tag", Integer.valueOf(request.getParameter("tag")));
		
		if(!StringUtils.isNull(request.getParameter("sex"))){
			paramMap.put("sex", request.getParameter("sex"));
		}
		
		paramMap.put("typename", URLDecoder.decode(request.getParameter("typename"), "UTF-8"));
		
		paramMap.put("midtypename", URLDecoder.decode(request.getParameter("midtypename"), "UTF-8"));
		
		int totalCount = lifeService.lifeInterestCount(paramMap);
		
		paramMap.put("totalCount", totalCount);
		
		List<Object> ret = lifeService.lifeInterestItem(paramMap);
		
		return generateData(ret,ret.size());
	}
	
	@RequestMapping("/lifeMedia")
	public  @ResponseBody FacadeResult lifeMedia(HttpServletRequest request){
		
		if(StringUtils.isNull(request.getParameter("tag"))||
					StringUtils.isNull(request.getParameter("type"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		if(!StringUtils.isNull(request.getParameter("sex"))){
			paramMap.put("sex", request.getParameter("sex"));
		}
		
		List<Object> ret = lifeService.lifeMedia(paramMap);
		//total count
		int totalCount = lifeService.getLifeMediaRecordCount(paramMap);
		
		return generateData(ret,totalCount);
	}
	
	/**
	 * 获取用户性别占比
	 */
	@RequestMapping("/getSexDistribution")
	public @ResponseBody FacadeResult getSexDistribution(HttpServletRequest request){
		if(StringUtils.isNull(request.getParameter("tag"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		//convert to parameterMap to genenral map
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("tag",Integer.valueOf(request.getParameter("tag")));
		if(!StringUtils.isNull(request.getParameter("sex"))){
			paramMap.put("sex", request.getParameter("sex"));
		}
		List<Object> ret = lifeService.getSexDistribution(paramMap);
		
		return generateData(ret);
		
	}
	/**
	 * 获取用户地区占比
	 * @param request
	 * @return
	 */
	
	@RequestMapping("/getAreaDistribution")
	public @ResponseBody FacadeResult getAreaDistribution(HttpServletRequest request){
		if(StringUtils.isNull(request.getParameter("tag"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		//convert to parameterMap to genenral map
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("tag",Integer.valueOf(request.getParameter("tag")));
		if(!StringUtils.isNull(request.getParameter("sex"))){
			paramMap.put("sex", request.getParameter("sex"));
		}
		
		List<Object> ret = lifeService.getAreaDistribution(paramMap);
		
		return generateData(ret);
		
	}
	
	@RequestMapping("/lifeCommonPage")
	public @ResponseBody FacadeResult lifeCommonPage(HttpServletRequest request){
		if(StringUtils.isNull(request.getParameter("tag"))||
					StringUtils.isNull(request.getParameter("type"))){
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		Map paramMap = StringUtils.getParameterMap(request);
		if(StringUtils.isNull((String)paramMap.get("num"))){
			paramMap.put("num","1");
		}
		if(StringUtils.isNull((String)paramMap.get("size"))){
			paramMap.put("size","10");
		}
		
		Page<Object> page = new Page(Integer.valueOf((String)paramMap.get("num")),Integer.valueOf((String)paramMap.get("size")));
		page = lifeService.lifeCommonPage(page,paramMap);
		List<Object> ret = page.getResult();
		return generateData(ret,page.getTotal());
		
	}
	
	
	
/*	@RequestMapping("/testcity")
	public void testCity(HttpServletRequest request){
		List<Object> cityNames = lifeService.getCity();
		for(int i=0;i<cityNames.size();i++){
			String city = cityNames.get(i).toString();
			if(city!=null&&!"".equals(city)){
				String firstLetter = CityUtil.cn2py(city.substring(0,1)).toUpperCase();
				System.out.println("letter----------"+firstLetter);
				Map<String,String> paramMap = new HashMap<String,String>();
				paramMap.put("name", city);
				paramMap.put("letter", firstLetter);
				lifeService.testCity(paramMap);
				System.out.println("update:: city"+city);
			}
			
			
		}
		
	}*/
	
	
}
