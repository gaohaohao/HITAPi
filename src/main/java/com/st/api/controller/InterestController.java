package com.st.api.controller;

import static java.util.stream.Collectors.toList;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.st.api.bo.FacadeResult;
import com.st.api.constants.ReturnServiceCode;
import com.st.api.service.IInterestService;
import com.st.api.util.StringUtils;

@RestController
@RequestMapping(value="/interest",produces = "application/json")
@Scope("prototype")
public class InterestController extends BaseController{

	@Autowired
	private IInterestService interestService;
	
	@RequestMapping("/getFunctionType")
	public FacadeResult getFunctionType(HttpServletRequest request) throws UnsupportedEncodingException{

		//validate not null parameter
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		

		
		List<Object> ret = interestService.getFunctionType(URLDecoder.decode(request.getParameter("prodcat"), "UTF-8"));
		
		return generateData(ret);
	}
	
	@RequestMapping("/interestAnalysisFavorite")
	public FacadeResult interestAnalysisFavorite(HttpServletRequest request){
		//validate not null parameter 
		if(StringUtils.isNull(request.getParameter("prodcat"))||
					StringUtils.isNull(request.getParameter("datefrom"))||
						StringUtils.isNull(request.getParameter("dateto"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
			
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		
		if(paramMap.get("cities")!=null&&!"".equals(paramMap.get("cities"))&&!"null".equals(paramMap.get("cities"))&&!"undefined".equals(paramMap.get("cities"))){
			
			
			paramMap.put("cities", StringUtils.strToIntArray((String)paramMap.get("cities"),","));
			
		}
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		
		List<Object> ret = interestService.interestAnalysisFavorite(paramMap);
		
 		return generateData(ret);
		
	}
	@RequestMapping("/getFunctionList")
	public FacadeResult getFunctionList(HttpServletRequest request) throws UnsupportedEncodingException{
		
		//validate not null parameter 
		if(StringUtils.isNull(request.getParameter("prodcat"))||
					StringUtils.isNull(request.getParameter("datefrom"))||
						StringUtils.isNull(request.getParameter("dateto"))||
							StringUtils.isNull(request.getParameter("type"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		
		paramMap.put("type",URLDecoder.decode(request.getParameter("type"), "UTF-8"));
		
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		if(paramMap.get("cities")!=null&&!"".equals(paramMap.get("cities"))&&!"null".equals(paramMap.get("cities"))&&!"undefined".equals(paramMap.get("cities"))){
			
			paramMap.put("cities", StringUtils.strToIntArray((String)paramMap.get("cities"), ","));
			/*String[] citiesStr = paramMap.get("cities").toString().split(",");
			Integer[] cities = new Integer[citiesStr.length];
			
			for(int i=0;i<cities.length;i++){
				cities[i] = Integer.valueOf(citiesStr[i]);
			}
			paramMap.put("cities", cities);*/
			
		}
		//cal value total count
		Integer totalCount = interestService.getFunctionTotalCount(paramMap);
		
		List<Object> ret = new ArrayList<>();
		
		if(totalCount != null){
			
			paramMap.put("totalCount",totalCount);
			
			ret = interestService.getFunctionList(paramMap);
		}
		
					
		return generateData(ret,totalCount);
				
		
	}
	@RequestMapping("/interestFunctionTrend")
	public Map interestFunctionTrend(HttpServletRequest request) throws UnsupportedEncodingException{
		
		//validate not null parameter 
		if(StringUtils.isNull(request.getParameter("prodcat"))||
					StringUtils.isNull(request.getParameter("datefrom"))||
						StringUtils.isNull(request.getParameter("dateto"))||
							StringUtils.isNull(request.getParameter("type"))){
			
			Map errorMap = StringUtils.returnMap(ReturnServiceCode.CHECK_DATA_ERROR, "Calibration parameter error");
			return errorMap;
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		
		paramMap.put("type",URLDecoder.decode(request.getParameter("type"), "UTF-8"));
		//get month list
		//List<Object> monthList = interestService.getMonthList(paramMap);
		//get function list
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		if(paramMap.get("cities")!=null&&!"".equals(paramMap.get("cities"))&&!"null".equals(paramMap.get("cities"))&&!"undefined".equals(paramMap.get("cities"))){
			
			paramMap.put("cities", StringUtils.strToIntArray((String)paramMap.get("cities"), ","));
			
		}
		List<Object> funcList = interestService.getInterestFuncList(paramMap);
		
		Map<String,Object> returnMap = StringUtils.returnMap(200, "");
		
		List<Object> ret = new ArrayList<Object>();
		
		for(int i=0;i<funcList.size();i++){
			
			Map<String,Object> valueMap = new HashMap<String,Object>();
			
			paramMap.put("function", funcList.get(i));
			
			List<Object> trendList = interestService.interestFunctionTrend(paramMap);
			
			valueMap.put("name", funcList.get(i));
			
			valueMap.put("value",trendList);
			
			ret.add(valueMap);
			
		}
		returnMap.put("data",ret);
		
		//cal value total count
		Integer totalCount = interestService.getFunctionTotalCount(paramMap);
		returnMap.put("results",totalCount);
		
		
		//List<HashMap> trendList = new ArrayList<HashMap>();
		/*Predicate<Map> filter = (innerMap) -> (Integer.valueOf(innerMap.get("month").toString())==1);
		for(int i=0;i<monthList.size();i++){
			for(int j=0;j<funcList.size();j++){
				int month = i;
				String func = (String) funcList.get(j);
				int value = 0;
				Map<String,Object> map = trendList.stream().filter((innerMap) -> (Integer.valueOf(innerMap.get("month").toString())==month)&&innerMap.get("prop").equals(func)).collect(toList()).get(1);
				map.get("prop");
					
			}
		}
		*/
		
		return returnMap;
	}
	
	
	
	
	
}
