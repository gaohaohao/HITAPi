package com.st.api.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.st.api.bo.FacadeResult;
import com.st.api.bo.Page;
import com.st.api.constants.ReturnServiceCode;
import com.st.api.mapper.IActiveQueryMapper;
import com.st.api.service.IActiveQueryService;
import com.st.api.util.StringUtils;

@Controller
@RequestMapping(value = "/active", produces = "application/json")
@Scope("prototype")
public class ActiveQueryController extends BaseController{

	private static Logger logger = LoggerFactory.getLogger(ActiveQueryController.class);
	
	@Autowired
	private IActiveQueryService activeQueryService;
	
	
	/**
	 * 获取省份列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getCityByProvince",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getCityByProvince(HttpServletRequest request){
	
			if(StringUtils.isNull(request.getParameter("prv_id"))){
				
				return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
			}
			
			List<Object> ret = activeQueryService.getCityByProvince(Integer.valueOf(request.getParameter("prv_id")));
			
			return generateData(ret);
	}
	
	/**
	 * 获取省份列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getCityByLevel",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getCityByLevel(HttpServletRequest request){
	
			if(StringUtils.isNull(request.getParameter("citylevel"))){
				
				return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
			}
			
			List<Object> ret = activeQueryService.getCityByLevel(Integer.valueOf(request.getParameter("citylevel")));
			
			return generateData(ret);
			
	}
	
	/**
	 * 获取省份列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getUserList",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getUserList(HttpServletRequest request){
		//convert to parameterMap to genenral map
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		Map paramMap = StringUtils.getParameterMap(request);
		
		if(StringUtils.isNull((String)paramMap.get("num"))){
			paramMap.put("num","1");
		}
		if(StringUtils.isNull((String)paramMap.get("size"))){
			paramMap.put("size","10");
		}
		
		if(paramMap.get("cities")!=null&&!"".equals(paramMap.get("cities"))&&!"null".equals(paramMap.get("cities"))&&!"undefined".equals(paramMap.get("cities"))){
	
			paramMap.put("cities", StringUtils.strToIntArray((String)paramMap.get("cities"),","));
			
		}
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			
			String[] prodcodeStr = paramMap.get("prodcode").toString().split(",");
			Integer[] prodcode = new Integer[prodcodeStr.length];
			
			for(int i=0;i<prodcode.length;i++){
				prodcode[i] = Integer.valueOf(prodcodeStr[i]);
			}
			paramMap.put("prodcode", prodcode);
			
		}
		Page page = new Page(Integer.valueOf((String)paramMap.get("num")),Integer.valueOf((String)paramMap.get("size")));
		
		page = activeQueryService.getUserList(page,paramMap);
		
		List<Object> ret = page.getResult();
		
		return generateData(ret,page.getTotal());

	}
	
	@RequestMapping(value="/getWIDHaier",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getWIDHaier(HttpServletRequest request){
		
		Map paramMap = StringUtils.getParameterMap(request);
		
		
		if(StringUtils.isNull((String)paramMap.get("num"))){
			paramMap.put("num","1");
		}
		if(StringUtils.isNull((String)paramMap.get("size"))){
			paramMap.put("size","10");
		}
		
		Page page = new Page(Integer.valueOf((String)paramMap.get("num")),Integer.valueOf((String)paramMap.get("size")));
		
		
		page = activeQueryService.getWIDHaier(page,paramMap);
		
		List<Object> ret = page.getResult();
		
		return generateData(ret,page.getTotal());
	}
	/**
	 * 获取社会化达人报表数据
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getDomainData",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getDomainData(HttpServletRequest request){
		
		List<Object> ret = activeQueryService.getDomainData();
		
		return generateData(ret,ret.size());
	}
	
	@RequestMapping(value="/getDomainUserList",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getDomainUserList(HttpServletRequest request) throws UnsupportedEncodingException{
		
		if(StringUtils.isNull(request.getParameter("domain"))||
				StringUtils.isNull(request.getParameter("v"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		Map paramMap = StringUtils.getParameterMap(request);
		paramMap.put("domain", URLDecoder.decode((String)paramMap.get("domain"), "UTF-8"));
		paramMap.put("v",Integer.valueOf((String)paramMap.get("v")) );
		
		//page param
		if(StringUtils.isNull((String)paramMap.get("num"))){
			paramMap.put("num","1");
		}
		if(StringUtils.isNull((String)paramMap.get("size"))){
			paramMap.put("size","10");
		}
		
		Page page = new Page(Integer.valueOf((String)paramMap.get("num")),Integer.valueOf((String)paramMap.get("size")));
		
		page = activeQueryService.getDomainUserList(page,paramMap);
		List<Object> ret = page.getResult();
		
		return generateData(ret,page.getTotal());
	}
	/**
	 * pc端接口获取收入分布信息
	 */
	@RequestMapping(value="/getIncomeDistribution",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getIncomeDistribution(HttpServletRequest request){
		
		//convert to parameterMap to genenral map
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		Map<String,Object> paramMap = processParamMap(request);
		List<Object>  ret = calPercent(activeQueryService.getIncomeDistribution(paramMap));
		
		
		return generateData(ret);
			
	}
	
	@RequestMapping(value="/getActivityDistribution",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getActivityDistribution(HttpServletRequest request){
		
		//convert to parameterMap to genenral map
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		Map<String,Object> paramMap = processParamMap(request);
		List<Object>  ret = calPercent(activeQueryService.getActivityDistribution(paramMap));
		
		return generateData(ret);
			
	}
	
	@RequestMapping(value="/getAgeDistribution",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getAgeDistribution(HttpServletRequest request){
		
		//convert to parameterMap to genenral map
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		Map<String,Object> paramMap = processParamMap(request);
		List<Object>  ret = calPercent(activeQueryService.getAgeDistribution(paramMap));
		return generateData(ret);
			
	}
	
	@RequestMapping(value="/getSexDistribution",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getSexDistribution(HttpServletRequest request){
		
		//convert to parameterMap to genenral map
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		Map<String,Object> paramMap = processParamMap(request);
		List<Object>  ret = calPercent(activeQueryService.getSexDistribution(paramMap));
		
		return generateData(ret);
			
	}
	
	public Map<String,Object> processParamMap(HttpServletRequest request){
		Map paramMap = StringUtils.getParameterMap(request);
		
		if(StringUtils.isNull((String)paramMap.get("num"))){
			paramMap.put("num","1");
		}
		if(StringUtils.isNull((String)paramMap.get("size"))){
			paramMap.put("size","10");
		}
		
		if(paramMap.get("cities")!=null&&!"".equals(paramMap.get("cities"))&&!"null".equals(paramMap.get("cities"))&&!"undefined".equals(paramMap.get("cities"))){
	
			paramMap.put("cities", StringUtils.strToIntArray((String)paramMap.get("cities"),","));
			
		}
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			
			String[] prodcodeStr = paramMap.get("prodcode").toString().split(",");
			Integer[] prodcode = new Integer[prodcodeStr.length];
			
			for(int i=0;i<prodcode.length;i++){
				prodcode[i] = Integer.valueOf(prodcodeStr[i]);
			}
			paramMap.put("prodcode", prodcode);
			
		}
		return paramMap;
		
	}	
	
	/**
	 * 处理countnum 为百分数
	 * @param ret
	 * @return
	 */
	public List<Object> calPercent(List<Object> ret){
		long sumCount = 0;
		for(int i=0;i<ret.size();i++){
			Map<String,Object> innerMap = (Map<String, Object>) ret.get(i);
			sumCount += Long.valueOf(innerMap.get("countnum").toString());
		}
		for(int i=0;i<ret.size();i++){
			Map<String,Object> innerMap = (Map<String, Object>) ret.get(i);
			long countNum =Long.valueOf(innerMap.get("countnum").toString());
			long tempResult = getPercent(countNum, sumCount);
			innerMap.put("countnum", tempResult);
			innerMap.put("totalCount",countNum);
		}
		return ret;
	}
	/**
	 * 计算百分数
	 * @param x
	 * @param total
	 * @return
	 */
	public long getPercent(long x,long total){
		double x_double=x*1.0;  
		double tempresult=x_double/total;
		BigDecimal b = new BigDecimal(tempresult);
	    return (long) (b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()*100);
	}
	
	
}
