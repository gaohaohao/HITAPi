package com.st.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.st.api.bo.FacadeResult;
import com.st.api.constants.ReturnServiceCode;

public abstract class BaseController {

	private static Logger logger = LoggerFactory.getLogger(BaseController.class);

	/**
	 * 异常页面控制
	 * 
	 * @param runtimeException
	 * @return
	 */
	@ExceptionHandler(RuntimeException.class)
	public @ResponseBody FacadeResult runtimeExceptionHandler(RuntimeException runtimeException) {
		logger.error("controller runtimeException:" + runtimeException.getLocalizedMessage());
		runtimeException.printStackTrace();
		return generateException(ReturnServiceCode.SERVER_EXCEPTION);
	}

	/**
	 * 正常返回数据
	 * 
	 * @return
	 */
	public FacadeResult generateData(Object data) {
		return new FacadeResult(data);
	}
	
	public FacadeResult generateData(Object data,long size) {
		return new FacadeResult(data,size);
	}
	
	public FacadeResult generateData(Integer result, String message) {
		return new FacadeResult(result, message);
	}

	/**
	 * 返回异常信息
	 * 
	 * @param data
	 * @return
	 */
	public FacadeResult generateException(Integer result) {
		return new FacadeResult(result);
	}

	/**
	 * 返回异常信息 自定义
	 * 
	 * @param data
	 * @return
	 */
	public FacadeResult generateException(Integer result, String message) {
		return new FacadeResult(result, message);
	}

}
