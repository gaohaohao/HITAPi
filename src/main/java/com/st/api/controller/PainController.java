package com.st.api.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.st.api.bo.FacadeResult;
import com.st.api.bo.Page;
import com.st.api.constants.ReturnServiceCode;
import com.st.api.service.IPainService;
import com.st.api.util.StringUtils;

@RestController
@RequestMapping(value="/pain",produces = "application/json")
@Scope("prototype")
public class PainController extends BaseController{

	@Autowired
	private IPainService painService;
	
	
	@RequestMapping("/getProductByCode")
	public FacadeResult getProductByCode(HttpServletRequest request) throws UnsupportedEncodingException{
		
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		
		Map paramMap = StringUtils.getParameterMap(request);
		
		if(StringUtils.isNull((String)paramMap.get("num"))){
			paramMap.put("num","1");
		}
		if(StringUtils.isNull((String)paramMap.get("size"))){
			paramMap.put("size","30");
		}
		
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		if(!StringUtils.isNull((String) paramMap.get("code"))){
			
			paramMap.put("code",URLDecoder.decode((String) paramMap.get("code"), "UTF-8"));
		}
		Page page = new Page(Integer.valueOf((String)paramMap.get("num")),Integer.valueOf((String)paramMap.get("size")));
		
		page = painService.getProductByCode(page,paramMap);
		
		List<Object> ret = page.getResult();
		
		return generateData(ret,page.getTotal());
	}
	
	@RequestMapping("/getPainTypeData")
	public FacadeResult getPainTypeData(HttpServletRequest request){
		
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);


		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			
			String[] prodcodeStr = paramMap.get("prodcode").toString().split(",");
			Integer[] prodcode = new Integer[prodcodeStr.length];
			
			for(int i=0;i<prodcode.length;i++){
				prodcode[i] = Integer.valueOf(prodcodeStr[i]);
			}
			paramMap.put("prodcode", prodcode);
			
		}
		//cal total records count
		int totalCount = painService.painTypeDataCount(paramMap);
		
		List<Object> ret = painService.getPainTypeData(paramMap);
		return generateData(ret,totalCount);
	}
	
	@RequestMapping("/getPainItemData")
	public FacadeResult getPainItemData(HttpServletRequest request) throws UnsupportedEncodingException{
		
		if(StringUtils.isNull(request.getParameter("prodcat"))||
				StringUtils.isNull(request.getParameter("paintype"))){
					
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		
		paramMap.put("painsubtype", URLDecoder.decode((String) paramMap.get("paintype"), "UTF-8"));
		
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			
			paramMap.put("series", series);
			
		}
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			String [] prodcode = paramMap.get("prodcode").toString().split(",");
			paramMap.put("prodcode", prodcode);
		}
		//cal total records count
		int totalCount = painService.painItemDataCount(paramMap);
		
		List<Object> ret = painService.getPainItemData(paramMap);
				
		return generateData(ret,totalCount);
	}
	@RequestMapping(value="/getProductVersion",method=RequestMethod.GET)
	public @ResponseBody FacadeResult getProductVersion(HttpServletRequest request) throws UnsupportedEncodingException{
		
		if(StringUtils.isNull(request.getParameter("prodcat"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			String [] prodcode = paramMap.get("prodcode").toString().split(",");
			paramMap.put("prodcode", prodcode);
		}
		if(paramMap.get("phenomenaDesc")!=null&&!"".equals(paramMap.get("phenomenaDesc"))&&!"null".equals(paramMap.get("phenomenaDesc"))&&!"phenomenaDesc".equals(paramMap.get("series"))){
			
			paramMap.put("phenomenaDesc", URLDecoder.decode((String) paramMap.get("phenomenaDesc"),"UTF-8"));
			
		}	
		paramMap.put("paintype", URLDecoder.decode((String) paramMap.get("paintype"), "UTF-8"));
		List<Object> ret = painService.getProductVersionTop5(paramMap);

		
		return generateData(ret);
		
	}	
	
	@RequestMapping("/painCompareByMonth")
	public @ResponseBody FacadeResult painCompareByMonth(HttpServletRequest request){
		
		if(StringUtils.isNull(request.getParameter("datefrom"))||
				StringUtils.isNull(request.getParameter("dateto"))){
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			String [] prodcode = paramMap.get("prodcode").toString().split(",");
			paramMap.put("prodcode", prodcode);
		}
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		List<Object> painTypeList = painService.getPainType(paramMap);
		List<Object> ret = new ArrayList<Object>();
		for(int i=0;i<painTypeList.size();i++){
			Map<String,Object> painValueMap = new HashMap<String,Object>();
			String pain_sub_type = (String) painTypeList.get(i);
			paramMap.put("pain_sub_type", pain_sub_type);
			List<Object> painMonthValue = painService.getPainMonthValue(paramMap);
			painValueMap.put("name", pain_sub_type);
			painValueMap.put("value", painMonthValue);
			ret.add(painValueMap);
		}
		
		return generateData(ret);
		
	}
	@RequestMapping("/painCompareByVersion")
	public @ResponseBody FacadeResult painCompareByVersion(HttpServletRequest request){
		if(StringUtils.isNull(request.getParameter("prodcode"))){
			
			return generateException(ReturnServiceCode.CHECK_DATA_ERROR);
		}
		//convert to parameterMap to genenral map
		Map paramMap = StringUtils.getParameterMap(request);
		String[] prodcode = new String[]{};
		if(paramMap.get("prodcode")!=null&&!"".equals(paramMap.get("prodcode"))&&!"null".equals(paramMap.get("prodcode"))&&!"undefined".equals(paramMap.get("prodcode"))){
			prodcode = paramMap.get("prodcode").toString().split(",");
			paramMap.put("prodcode", prodcode);
		}
		if(paramMap.get("series")!=null&&!"".equals(paramMap.get("series"))&&!"null".equals(paramMap.get("series"))&&!"undefined".equals(paramMap.get("series"))){
			
			String[] series = paramMap.get("series").toString().split(",");
			
			paramMap.put("series", series);
			
		}
		List<Object> ret = new ArrayList<Object>();
		for(String prodCode : prodcode){
			paramMap.put("prodCode",prodCode);
			List<Object> painValueList = painService.painCompareByVersion(paramMap);
			String productTag = painService.getProductTagById(prodCode);
			Map<String,Object> dataMap = new HashMap<String,Object>();
			dataMap.put("codeName", productTag);
			dataMap.put("dataValue", painValueList);
			ret.add(dataMap);
		}
		return generateData(ret);
		
	}
	
}
