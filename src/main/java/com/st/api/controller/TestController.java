package com.st.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.st.api.service.impl.TestService;

@RestController
@RequestMapping(value="/hello",produces = "application/json")
@Scope("prototype")
public class TestController extends BaseController{

	@Autowired
	private TestService testService;
	
	@RequestMapping("/testTx")
	public void testTransaction(){
		testService.updateTemp();
	}
}
