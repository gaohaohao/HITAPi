package com.st.api.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.mapper.ISystemQueryMapper;
import com.st.api.mapper2.IMessageMapper;
import com.st.api.service.IMessageService;
import com.st.api.service.ISystemQueryService;
@Service
public class MessageService implements IMessageService{

	@Autowired
	private IMessageMapper messageMapper;
	@Autowired
	private ISystemQueryService systemQueryService;
	@Override
	public void sendMessage(List<Map> userMapList,Map paramMap) {
	
		int actId = messageMapper.getActId();
		userMapList.stream().forEach(innerMap ->{
			innerMap.put("offerId", actId);
			innerMap.put("dt", paramMap.get("dt"));
		});
		paramMap.put("actId", actId);
		messageMapper.insertJOF(paramMap);
		
		messageMapper.insertJCUST(userMapList);
		
		
		//update active_user_f
		String [] mobiles = (String[]) paramMap.get("mobiles");
		List<Object> mobileList = new ArrayList<Object>();
		for(String mobile:mobiles){
			Map<String,Object> mobileMap = new HashMap<String,Object>(); 
			mobileMap.put("offerId", actId);
			mobileMap.put("mobile", mobile);
			mobileList.add(mobileMap);
			
		}
		
		
		messageMapper.insertTestMobile(mobileList);
		systemQueryService.updateAUFByCId(userMapList);
		
		
	}
	

}
