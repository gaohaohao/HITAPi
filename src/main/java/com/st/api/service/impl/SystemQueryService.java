package com.st.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.bo.Page;
import com.st.api.interceptor.PageInterceptor;
import com.st.api.mapper.ISystemQueryMapper;
import com.st.api.service.ISystemQueryService;
@Service
public class SystemQueryService implements ISystemQueryService{

	@Autowired
	private ISystemQueryMapper systemQueryMapper;
	
	
	@Override
	public void updateEcInfo(Map paramMap) {
		
		systemQueryMapper.updateEcInfo(paramMap);
		
	}


	@Override
	public List<Object> getEcList() {
		
		return systemQueryMapper.getEcList();
	}


	@Override
	public List<Object> getProdCatListByRole(Map paramMap) {
		
		return systemQueryMapper.getProdCatListByRole(paramMap);
	}


	@Override
	public List<Object> getIncList() {
		
		return systemQueryMapper.getIncList();
	}


	@Override
	public List<Object> getEcListByRole(String uid) {
		
		return systemQueryMapper.getEcListByRole(uid);
	}


	@Override
	public List<Object> getProdCatList(Map paramMap) {
		
		return systemQueryMapper.getProdCatList(paramMap);
	}


	@Override
	public void updateRcInfo(Map<String, Object> rcMap) {
		systemQueryMapper.updateRcInfo(rcMap);
		
	}


	@Override
	public List<Object> getProdCatByUserCode(String uid) {
		
		return systemQueryMapper.getProdCatByUserCode(uid);
	}


	@Override
	public List<Object> getUserMenuByUserCode(String uid) {
		
		return systemQueryMapper.getUserMenuByUserCode(uid);
	}


	@Override
	public Map<String, Object> getUserInfoByUserCode(String uid) {
		
		return systemQueryMapper.getUserInfoByUserCode(uid);
	}


	@Override
	public List<Object> getProdSeries(String prodcat) {
		
		return systemQueryMapper.getProdSeries(prodcat);
	}


	@Override
	public List<Object> getProvince() {
	
		return systemQueryMapper.getProvince();
	}


	@Override
	public List<Object> getRoleList() {
		
		return systemQueryMapper.getRoleList();
	}


	@Override
	public Map<String, Object> getAccessInfoMap(String uid) {
		
		return systemQueryMapper.getAccessInfoMap(uid);
	}


	


	@Override
	public List<Object> getProdCatListByRole2(Map<String, Object> paramMap) {
		
		return systemQueryMapper.getProdCatListByRole2(paramMap);
	}


	@Override
	public void updateAllEcInfo(Map<String, Object> ecMap) {
		
		 systemQueryMapper.updateAllEcInfo(ecMap);
	}


	@Override
	public List<Map> getUserListByCondition(Map<String, Object> conditionMap) {
		
		return systemQueryMapper.getUserListByCondition(conditionMap);
	}


	


	@Override
	public List<Object> getSysUserRole(String usercode) {
		
		return systemQueryMapper.getSysUserRole(usercode);
	}


	@Override
	public Page<Object> getSysUserList(Page page,Map<String, Object> paramMap) {
		PageInterceptor.startPage(page.getPageNum(), page.getPageSize());
		List<Object> sysUserList = systemQueryMapper.getSysUserList(paramMap);
		return PageInterceptor.endPage();
	}




	@Override
	public void updateUserRoleAdvice(String usercode, List<Map> roleMapList,boolean hasSHMPower) {
		//先删除用户角色
		systemQueryMapper.delUserRole(usercode);
		//如果有审核权限
		if(hasSHMPower){
			//删除所有人的审核权限
			systemQueryMapper.delSHMPower();
		}
		//更新用户角色
		systemQueryMapper.updateUserRole(roleMapList);
		
		
	}


	@Override
	public void delSysUserAdvice(String usercode) {
		
		systemQueryMapper.delUserDataByUC(usercode);
		
		systemQueryMapper.delUserRoleRelByUC(usercode);
		
		systemQueryMapper.delUserInfoByUC(usercode);
		
		
	}


	@Override
	public List<Object> getDeptUseTop5(Map<String, Object> paramMap) {
		
		return systemQueryMapper.getDeptUseTop5(paramMap);
	}


	@Override
	public List<Object> userUseInfo(Map<String, Object> paramMap) {
		
		return systemQueryMapper.userUseInfo(paramMap);
	}


	@Override
	public List<Object> getIncreaseUserTop5(Map<String, Object> paramMap) {
	
		return systemQueryMapper.getIncreaseUserTop5(paramMap);
	}


	@Override
	public List<Object> increaseUserInfo(Map<String, Object> paramMap) {
		
		return systemQueryMapper.increaseUserInfo(paramMap);
	}


	@Override
	public List<Object> userRoleUseInfo(Map<String, Object> paramMap) {
		
		return systemQueryMapper.userRoleUseInfo(paramMap);
	}


	@Override
	public List<Object> increaseUserRoleInfo(Map<String, Object> paramMap) {
		
		return systemQueryMapper.increaseUserRoleInfo(paramMap);
	}


	@Override
	public void updateAUFByCId(List<Map> userMapList) {
		
		 systemQueryMapper.updateAUFByCId(userMapList);
	}


	@Override
	public List<Map> getUserListByConditionRandom(Map<String, Object> conditionMap) {
		
		return systemQueryMapper.getUserListByConditionRandom(conditionMap);
	}


	



}
