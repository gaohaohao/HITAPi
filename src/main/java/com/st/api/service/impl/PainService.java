package com.st.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.bo.Page;
import com.st.api.interceptor.PageInterceptor;
import com.st.api.mapper.IPainMapper;
import com.st.api.service.IPainService;

@Service
public class PainService implements IPainService{

	@Autowired
	private IPainMapper painMapper;
	
	@Override
	public List<Object> getPainTypeData(Map paramMap) {
		
		return painMapper.getPainTypeData(paramMap);
	}

	@Override
	public int painTypeDataCount(Map paramMap) {
		
		return painMapper.painTypeDataCount(paramMap);
	}

	@Override
	public int painItemDataCount(Map paramMap) {
		
		return painMapper.painItemDataCount(paramMap);
	}

	@Override
	public List<Object> getPainItemData(Map paramMap) {
		
		return painMapper.getPainItemData(paramMap);
	}

	@Override
	public Page<Object> getProductByCode(Page page, Map paramMap) {
		PageInterceptor.startPage(page.getPageNum(), page.getPageSize());
		List<Object> prodPage = painMapper.getProductByCode(paramMap);
		return PageInterceptor.endPage();
	}

	@Override
	public List<Object> getProductVersionTop5(Map paramMap) {
		
		return painMapper.getProductVersionTop5(paramMap);
	}

	@Override
	public List<Object> getPainType(Map paramMap) {
		
		return painMapper.getPainType(paramMap);
	}

	@Override
	public List<Object> getPainMonthValue(Map paramMap) {
		
		return painMapper.getPainMonthValue(paramMap);
	}

	@Override
	public List<Object> painCompareByVersion(Map paramMap) {
		
		return painMapper.painCompareByVersion(paramMap);
	}

	@Override
	public String getProductTagById(String prodCode) {
		
		return painMapper.getProductTagById(prodCode);
	}

	
}
