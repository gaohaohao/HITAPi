package com.st.api.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.mapper.IUserMapper;
import com.st.api.service.IUserService;
@Service
public class UserService implements IUserService{

	@Autowired
	private IUserMapper userMapper;
	
	@Override
	public int isFirstLogin(String userid) {
		return userMapper.isFirstLogin(userid);
	}

	

	@Override
	public void insertSysLoginDetail(Map<String, String> insertParamMap) {
		userMapper.insertSysLoginDetail(insertParamMap);
		
	}



	@Override
	public int hasInSertUserInfo(String userid) {
		return userMapper.hasInSertUserInfo(userid);
		
		
	}



	@Override
	public void insertUserInfo(Map<String, String> userinfoMap) {
		
		userMapper.insertUserInfo(userinfoMap);	
	}



	@Override
	public void updateUserInfoLastLoginTime(Map<String, String> userinfoMap) {
		
		userMapper.updateUserInfoLastLoginTime(userinfoMap);
		
	}



	@Override
	public Map<String, Object> getAppVersion() {
		
		return userMapper.getAppVersion();
	}

}
