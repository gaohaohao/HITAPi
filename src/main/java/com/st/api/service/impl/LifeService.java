package com.st.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.bo.Page;
import com.st.api.interceptor.PageInterceptor;
import com.st.api.mapper.ILifeMapper;
import com.st.api.service.ILifeService;
@Service
public class LifeService implements ILifeService{
	
	@Autowired
	private ILifeMapper lifeMapper;

	@Override
	public List<Object> getLifeTag() {
		
		return lifeMapper.getLifeTag();
	}


	@Override
	public List<Object> lifeInterestAnalysis(Map<String, Object> paramMap) {
		
		return lifeMapper.lifeInterestAnalysis(paramMap);
	}


	@Override
	public List<Object> lifeInterestItemCycle(Map<String, Object> paramMap) {
		
		return lifeMapper.lifeInterestItemCycle(paramMap);
	}


	@Override
	public List<Object> lifeInterestItem(Map<String, Object> paramMap) {
		
		return	lifeMapper.lifeInterestItem(paramMap);
	}


	@Override
	public List<Object> lifeMedia(Map paramMap) {
		
		return lifeMapper.lifeMedia(paramMap);
	}


	@Override
	public int lifeInterestCount(Map paramMap) {
		
		return lifeMapper.lifeInterestCount(paramMap);
	}


	@Override
	public int getLifeMediaRecordCount(Map paramMap) {
		
		return lifeMapper.getLifeMediaRecordCount(paramMap);
	}


	@Override
	public List<Object> getSexDistribution(Map<String, Object> paramMap) {
		
		return lifeMapper.getSexDistribution(paramMap);
	}


	@Override
	public List<Object> getAreaDistribution(Map paramMap) {
		
		return lifeMapper.getAreaDistribution(paramMap);
	}


	@Override
	public Page<Object> lifeCommonPage(Page page, Map paramMap) {
		PageInterceptor.startPage(page.getPageNum(), page.getPageSize());
		List<Object> lifeCommonPage = lifeMapper.lifeCommonPage(paramMap);
		return PageInterceptor.endPage();
	}


	/*@Override
	public void testCity(Map paramMap) {
		lifeMapper.testCity(paramMap);
		
	}


	@Override
	public List<Object> getCity() {
		
		return lifeMapper.getCity();
	}*/

}
