package com.st.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.mapper.IInterestMapper;
import com.st.api.service.IInterestService;
@Service
public class InterestService implements IInterestService{

	@Autowired
	private IInterestMapper interestMapper;
	
	@Override
	public List<Object> getFunctionType(String prodcat) {
		
		return interestMapper.getFunctionType(prodcat);
	}

	@Override
	public List<Object> interestAnalysisFavorite(Map paramMap) {
		
		return interestMapper.interestAnalysisFavorite(paramMap);
	}

	@Override
	public List<Object> getFunctionList(Map paramMap) {
		return interestMapper.getFunctionList(paramMap);
	}

	@Override
	public Integer getFunctionTotalCount(Map paramMap) {
		
		return interestMapper.getFunctionTotalCount(paramMap);
	}

	@Override
	public List<Object> getMonthList(Map paramMap) {
		
		return interestMapper.getMonthList(paramMap);
	}

	@Override
	public List<Object> getInterestFuncList(Map paramMap) {
		
		return interestMapper.getInterestFuncList(paramMap);
	}

	@Override
	public List<Object> interestFunctionTrend(Map paramMap) {
		
		return interestMapper.interestFunctionTrend(paramMap);
	}

	

}
