package com.st.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.st.api.bo.Page;
import com.st.api.interceptor.PageInterceptor;
import com.st.api.mapper.IActiveQueryMapper;
import com.st.api.service.IActiveQueryService;
@Service
public class ActiveQueryService implements IActiveQueryService{

	@Autowired
	private IActiveQueryMapper activeQueryMapper;
	
	
	@Override
	public List<Object> getCityByProvince(Integer prvId) {
		
		return activeQueryMapper.getCityByProvince(prvId);
	}

	@Override
	public List<Object> getCityByLevel(Integer levelId) {
		
		return activeQueryMapper.getCityByLevel(levelId);
	}

	@Override
	public Page<Object> getUserList(Page page, Map paramMap) {
		PageInterceptor.startPage(page.getPageNum(), page.getPageSize());
		List<Object> userPage = activeQueryMapper.getUserList(paramMap);
		return PageInterceptor.endPage();
	}

	@Override
	public Page<Object> getWIDHaier(Page page, Map paramMap) {
		PageInterceptor.startPage(page.getPageNum(), page.getPageSize());
		List<Object> widPage = activeQueryMapper.getWIDHaier(paramMap);
		return PageInterceptor.endPage();
	}

	@Override
	public List<Object> getDomainData() {
		
		return activeQueryMapper.getDomainData();
	}



	@Override
	public  Page<Object> getDomainUserList(Page page, Map paramMap) {
		PageInterceptor.startPage(page.getPageNum(), page.getPageSize());
		List<Object> domainUserPage = activeQueryMapper.getDomainUserList(paramMap);
		return PageInterceptor.endPage();
	}

	@Override
	public List<Object> getIncomeDistribution(Map paramMap) {
		
		return activeQueryMapper.getIncomeDistribution(paramMap);
	}

	@Override
	public List<Object> getActivityDistribution(Map<String, Object> paramMap) {
		
		return activeQueryMapper.getActivityDistribution(paramMap);
	}

	@Override
	public List<Object> getAgeDistribution(Map<String, Object> paramMap) {
		
		return activeQueryMapper.getAgeDistribution(paramMap);
	}

	@Override
	public List<Object> getSexDistribution(Map<String, Object> paramMap) {
		
		return activeQueryMapper.getSexDistribution(paramMap);
	}

}
