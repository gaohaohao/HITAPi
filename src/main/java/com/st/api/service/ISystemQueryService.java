package com.st.api.service;

import java.util.List;
import java.util.Map;

import com.st.api.bo.Page;

public interface ISystemQueryService {

	/**
	 * 更新生态圈和产品大类
	 * @param uid
	 * @param ecid
	 * @param prodcat
	 * @return
	 */
	public void updateEcInfo(Map paramMap);

	/**
	 * 获取生态圈列表
	 * @return
	 */
	public List<Object> getEcList();

	/**
	 * 获取产品大类列表(当前用户)
	 * @param valueOf
	 * @return
	 */
	public List<Object> getProdCatListByRole(Map paramMap);

	/**
	 * 获取收入等及列表
	 * @return
	 */
	public List<Object> getIncList();

	/**
	 * 获取生态圈列表（当前用户）
	 * @param parameter
	 * @return
	 */
	public List<Object> getEcListByRole(String uid);

	/**
	 * 获取产品大类列表
	 * @return
	 */
	public List<Object> getProdCatList(Map paramMap);

	/**
	 * 更新职能
	 * @param rcMap
	 */
	public void updateRcInfo(Map<String, Object> rcMap);

	/**
	 * 通过usercode查询出大类
	 * @param parameter
	 * @return
	 */
	public List<Object> getProdCatByUserCode(String uid);

	/**
	 * 通过usercode查处用户菜单
	 * @param parameter
	 * @return
	 */
	public List<Object> getUserMenuByUserCode(String uid);

	

	/**
	 * 通过usercode 获得用户信息
	 * @param parameter
	 * @return
	 */
	public Map<String, Object> getUserInfoByUserCode(String parameter);

	/**
	 * 获取产品系列
	 * @param parameter
	 * @return
	 */
	public List<Object> getProdSeries(String parameter);
	
	
	/**
	 * 获取省份列表
	 * @return
	 */
	public List<Object> getProvince();

	/**
	 * 	获取用户职能列表
	 * @return
	 */
	public List<Object> getRoleList();

	/**
	 * 获取访问信息map
	 * @return
	 */
	public Map<String, Object> getAccessInfoMap(String uid);
	
	/**
	 * 4.2.5	获取产品大类列表（当前用户有权限的)2
	 * @return
	 */

	public List<Object> getProdCatListByRole2(Map<String, Object> paramMap);
	/**
	 * 更新生态圈和产品大类
	 * @param uid
	 * @param ecid
	 * @param prodcat
	 * @return
	 */
	public void updateAllEcInfo(Map<String, Object> ecMap);

	/**
	 * 通过condition查询用户列表
	 * @param conditionMap
	 * @return
	 */
	public List<Map> getUserListByCondition(Map<String, Object> conditionMap);

	/**
	 * 获取系统用户列表
	 * @param page 
	 * @return
	 */
	public Page<Object> getSysUserList(Page page, Map<String, Object> paramMap);

	
	public List<Object> getSysUserRole(String usercode);

	
	
	public void updateUserRoleAdvice(String usercode, List<Map> roleMapList, boolean hasSHMPower);

	/**
	 * 删除系统用户
	 * @param parameter
	 */
	public void delSysUserAdvice(String parameter);

	/**
	 * 获取活跃用户数的前5个部门
	 * @param paramMap
	 * @return
	 */
	public List<Object> getDeptUseTop5(Map<String, Object> paramMap);

	/**
	 * 获取用户使用情况
	 * @param paramMap
	 * @return
	 */
	public List<Object> userUseInfo(Map<String, Object> paramMap);

	/**
	 * 新增用户top5
	 * @param paramMap
	 * @return
	 */
	public List<Object> getIncreaseUserTop5(Map<String, Object> paramMap);

	/**
	 * 新增用户top5
	 * @param paramMap
	 * @return
	 */
	public List<Object> increaseUserInfo(Map<String, Object> paramMap);

	/**
	 * 获取角色活跃用户
	 * @param paramMap
	 * @return
	 */
	public List<Object> userRoleUseInfo(Map<String, Object> paramMap);

	/**
	 * 获取角色新增用户
	 * @param paramMap
	 * @return
	 */
	public List<Object> increaseUserRoleInfo(Map<String, Object> paramMap);

	/**
	 * 更新推送时间
	 * @param userMapList
	 */
	public void updateAUFByCId(List<Map> userMapList);

	public List<Map> getUserListByConditionRandom(Map<String, Object> conditionMap);

	

	

	
}
