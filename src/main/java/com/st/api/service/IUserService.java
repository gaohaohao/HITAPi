package com.st.api.service;

import java.util.Map;

public interface IUserService {

	/**
	 * 判断用户是否是第一次登录
	 * @param userid
	 * @return
	 */
	public int isFirstLogin(String userid);

	

	/**
	 * 插入登录的详细信息
	 * @param userid
	 * @param ipAddr
	 */
	public void insertSysLoginDetail(Map<String, String> insertParamMap);


	/**
	 * 是否W_SYS_USER_ROLE_REL中已存在用户的信息
	 * @param string
	 */

	public int hasInSertUserInfo(String userid);


	/**
	 * 向W_SYS_USER_ROLE_REL中插入用户信息
	 * @param userinfoMap
	 */

	public void insertUserInfo(Map<String, String> userinfoMap);



	public void updateUserInfoLastLoginTime(Map<String, String> userinfoMap);


	/**
	 * 获取app版本
	 * @return
	 */
	public Map<String, Object> getAppVersion();

}
