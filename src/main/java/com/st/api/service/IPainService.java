package com.st.api.service;

import java.util.List;
import java.util.Map;

import com.st.api.bo.Page;


public interface IPainService {

	

	/**
	 * 4.3.2	根据查询条件获取痛点报表数据
	 * @param paramMap
	 * @return
	 */
	List<Object> getPainTypeData(Map paramMap);

	/**
	 * 计算痛点数据总条数
	 * @param paramMap
	 * @return
	 */
	int painTypeDataCount(Map paramMap);

	/**
	 * 4.3.3	选择痛点钻探获取痛点细分报表数据
	 * @param paramMap
	 * @return
	 */
	int painItemDataCount(Map paramMap);

	/**
	 *  4.3.3	选择痛点钻探获取痛点细分报表数据
	 * @param paramMap
	 * @return
	 */
	List<Object> getPainItemData(Map paramMap);

	/**
	 * 4.3.1	根据型号编码获取产品信息
	 * @param page
	 * @param paramMap
	 * @return
	 */
	Page<Object> getProductByCode(Page page, Map paramMap);

	/**
	 * 获取产品版本信息
	 * @param paramMap 
	 * @return
	 */
	List<Object> getProductVersionTop5(Map paramMap);

	/**
	 * 获取痛点类型
	 * @param paramMap
	 * @return
	 */
	List<Object> getPainType(Map paramMap);

	/**
	 * 痛点按月对比
	 * @param paramMap
	 * @return
	 */
	List<Object> getPainMonthValue(Map paramMap);

	/**
	 * 痛点型号对比
	 * @param paramMap
	 * @return
	 */
	List<Object> painCompareByVersion(Map paramMap);

	/**
	 * 通过id获取产品标签
	 * @param prodCode
	 * @return
	 */
	String getProductTagById(String prodCode);

	

}
