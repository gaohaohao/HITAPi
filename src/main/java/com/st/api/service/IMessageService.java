package com.st.api.service;

import java.util.List;
import java.util.Map;

public interface IMessageService {

	void sendMessage(List<Map> userMapList, Map paramMap);

}
