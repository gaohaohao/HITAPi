package com.st.api.service;

import java.util.List;
import java.util.Map;

import com.st.api.bo.Page;

public interface IActiveQueryService {

	

	/**
	 * 根据省份获取城市列表
	 * @param valueOf
	 * @return
	 */
	public List<Object> getCityByProvince(Integer prvId);

	/**
	 * 通过城市等级获取城市列表
	 * @param valueOf
	 * @return
	 */
	public List<Object> getCityByLevel(Integer levelId);

	/**
	 * 根据查询条件获取人员列表
	 * @param page
	 * @param paramMap
	 * @return
	 */
	public Page getUserList(Page page, Map paramMap);

	/**
	 * 获取社会化数据海尔相关
	 * @param page
	 * @param paramMap
	 * @return
	 */
	public Page getWIDHaier(Page page, Map paramMap);

	/**
	 * 获取社会化达人报表数据
	 * @return
	 */
	public List<Object> getDomainData();

	/**
	 * 	获取社会化数据领域达人微博ID
	 * @param decode
	 * @param valueOf
	 * @return
	 */
	public Page<Object> getDomainUserList(Page page,Map paramMap);

	/**
	 * 收入分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getIncomeDistribution(Map paramMap);

	/**
	 * 活跃度分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getActivityDistribution(Map<String, Object> paramMap);
	/**
	 * 年龄分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getAgeDistribution(Map<String, Object> paramMap);
	/**
	 * 性别分布
	 * @param paramMap
	 * @return
	 */
	public List<Object> getSexDistribution(Map<String, Object> paramMap);
}
