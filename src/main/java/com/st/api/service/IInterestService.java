package com.st.api.service;

import java.util.List;
import java.util.Map;

public interface IInterestService {

	/**
	 * 4.4.1	获取产品功能类别
	 * @param decode
	 * @return 
	 */
	List<Object> getFunctionType(String prodcat);

	/**
	 * 4.4.2	用户兴趣分析最喜欢
	 * @param paramMap
	 * @return
	 */
	List<Object> interestAnalysisFavorite(Map paramMap);

	/**
	 * 4.4.3	获取功能列表
	 * @param paramMap
	 * @return
	 */
	List<Object> getFunctionList(Map paramMap);

	/**
	 * cal total count
	 * @param paramMap
	 * @return
	 */
	Integer getFunctionTotalCount(Map paramMap);
	/**
	 * 4.4.4	产品功能趋势图 获取月份
	 * @param paramMap
	 * @return
	 */
	List<Object> getMonthList(Map paramMap);

	/**
	 *  4.4.4	产品功能趋势图  获取功能列表
	 * @param paramMap
	 * @return
	 */
	List<Object> getInterestFuncList(Map paramMap);

	/**
	 * 产品功能趋势图 
	 * @param paramMap
	 * @return
	 */
	List<Object> interestFunctionTrend(Map paramMap);

	
}
