package com.st.api.constants;

/**
 * 错误代码
 * 错误信息从文件中读取
 * @author wuxianwei
 *
 */
public final class ReturnServiceCode {
	private ReturnServiceCode() {
	}

	/**
	 * 通用1开头，后三位有效：001
	 */
	public static final int CHECK_DATA_ERROR = 1001;
	public static final int SERVER_EXCEPTION = 1002;
	public static final int DATA_ERROR = 1003;
	public static final int SERVICE_CALL_ERROR = 1004;
	public static final int SERVICE_EXECUTE_ERROR = 1005;

	//登录userid or password为空，错误返回码
	public static final int USERID_OR_PASSWORD_NULL =1006;
	//登录失败错误返回码
	public static final int LOGIN_FAILED_ERROR = 1007;
	//拦截器验证userid或token为空，错误返回码
	public static final int USERID_OR_TOKEN_NULL = 1008;
	//interceptor invalid token
	public static final int INVALID_TOKEN=1009;

	

}
