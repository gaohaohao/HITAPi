package com.st.api.test;

import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.st.api.util.HttpClientUtils;

public class Result {
    private String datas;
    private String desc;
    private Integer flag;
    private Long usetime;
    
    @Test
    public void testGetFilterUsersApi() {
        //long start = System.currentTimeMillis();
        try {
            PostMethod postMethod = new PostMethod(
                    "http://211.151.59.249:8091/cdapi/req?appid=444&passwd=746749378&method=getFilterUsers&param=%7B%22attr_fp%22%3A%5B%7B%22id%22%3A%221%22%2C%22dt%22%3A%221%22%2C%22value%22%3A%22%E5%8C%97%E4%BA%AC%22%7D%5D%2C%22rel_fp%22%3A%5B%5D%2C%22uuid%22%3A%221068966860%22%2C%22sort_attr_id%22%3A%2231%22%2C%22topn%22%3A%2250%22%2C%22save_type_id%22%3A%220%22%2C%22mindname%22%3A%22get_target_user%22%7D%0A");
            HttpClientUtils.httpClient.executeMethod(postMethod);
            String retStr = postMethod.getResponseBodyAsString();
            System.out.println(retStr);

            ObjectMapper mapper = new ObjectMapper();
            Result result = mapper.readValue(retStr, Result.class);
            
            if(result.getFlag() == 200){
                String retDatas = result.getDatas();
                //System.out.println(retDatas);
                String [] retDatasArray = retDatas.split(":");
                if(retDatasArray.length > 1){
                    String retUuids = retDatasArray[retDatasArray.length - 1];
                    if(retUuids.contains(";")){
                        String [] uuidArray = retUuids.split(";");
                        for (int i = 0; i < uuidArray.length; i++) {
                            System.out.println(uuidArray[i]);
                        }
                    }
                    
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println(System.currentTimeMillis() - start);
    }
    
    public String getDatas() {
        return datas;
    }
    public void setDatas(String datas) {
        this.datas = datas;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public Integer getFlag() {
        return flag;
    }
    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    public Long getUsetime() {
        return usetime;
    }
    public void setUsetime(Long usetime) {
        this.usetime = usetime;
    }
    
    public static void main(String[] args) {
		Result result = new Result();
		
		result.testGetFilterUsersApi();
	}

}
