package com.st.api.mq;


import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.st.api.bo.PullUserModel;
@Repository
public class MessageProduct {
	private static Logger logger = LoggerFactory.getLogger(MessageProduct.class);
	public MessageProduct() {
		
	}

	
	public static void pullActivityUser(PullUserModel pullUserModel){
		ConnectionFactory connFactory = JMSFactory.getJMSConnectionFactory();
		 try {
			//JMS 客户端到JMS Provider 的连接 
			Connection connection = connFactory.createConnection();
			connection.start();
			// Session： 一个发送或接收消息的线程 
			Session session = connection.createSession(Boolean.TRUE,Session.AUTO_ACKNOWLEDGE);
			// Destination ：消息的目的地;消息发送给谁. 
			// 获取session注意参数值my-queue是Query的名字 
			Destination destination = session.createQueue("active-user-queue");
			// MessageProducer：消息生产者 
			MessageProducer producer = session.createProducer(destination);
			// 设置失效时间
			
			//producer.setTimeToLive(3600 * 1000 * 24 * 2);
			//设置持久化 
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);
			//发送一条消息 
			ObjectMessage objectMessage = session.createObjectMessage(pullUserModel);
			//通过消息生产者发出消息 
			producer.send(objectMessage);
			session.commit();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}


}
