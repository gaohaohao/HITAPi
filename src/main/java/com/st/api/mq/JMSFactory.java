package com.st.api.mq;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class JMSFactory {
	private static ConnectionFactory connectionFactory; 
	/**
	 * 获得连接工厂
	 * */
	public static ConnectionFactory getJMSConnectionFactory() {
		if(connectionFactory == null){
			//可以设置设定好的用户名和密码
			 connectionFactory = new ActiveMQConnectionFactory( 
	                 ActiveMQConnection.DEFAULT_USER, 
	                 ActiveMQConnection.DEFAULT_PASSWORD, 
	                 "tcp://10.138.111.54:61616"); 
		}
		return connectionFactory;
	}

}
